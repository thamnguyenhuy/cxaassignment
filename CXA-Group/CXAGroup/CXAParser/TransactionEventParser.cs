﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using CXAModel;
using CXAUtil;

namespace CXAParser
{
    public class TransactionEventParserResult
    {
        public List<TransactionEvent> TransactionEvents { get; set; }
        public List<string> ErrorRows { get; set; }

        public TransactionEventParserResult()
        {
            TransactionEvents = new List<TransactionEvent>();
            ErrorRows = new List<string>();
        }
    }

    public class TransactionEventParser
    {
        private const int COLUMN_COUNT = 7;
        private const int COLUMN_TRANSACTION_ID = 0;
        private const int COLUMN_VERSION = 1;
        private const int COLUMN_ITEM_ID = 2;
        private const int COLUMN_QUANTITY = 3;
        private const int COLUMN_DIRECTION = 4;
        private const int COLUMN_ACCOUNT = 5;
        private const int COLUMN_OPEATION = 6;

        /// <summary>
        /// Parse the transaction event from text file to TransactionEventParserResult
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns>TransactionEventParserResult</returns>
        public static TransactionEventParserResult FromFile(string filepath)
        {
            TransactionEventParserResult result = new TransactionEventParserResult();
            try
            {
                //check filepath is not null && file is existing in system
                if (!string.IsNullOrEmpty(filepath) && File.Exists(filepath))
                {
                    using (StreamReader file = new StreamReader(filepath))
                    {
                        string row;
                        int count = 0;
                        while (!file.EndOfStream)
                        {
                            row = file.ReadLine();
                            if (!string.IsNullOrEmpty(row))
                            {
                                TransactionEvent item = FromRow(row);
                                if (item != null)
                                {
                                    result.TransactionEvents.Add(item);
                                }
                                else
                                {
                                    result.ErrorRows.Add(string.Format("Row {0}: {1}", count, row));
                                    //add to the error list
                                }
                                count++;
                            }
                        }

                        //sort the record by account number, itemid, transaction id and version id
                        result.TransactionEvents = result.TransactionEvents.OrderBy(a => a.TransactionId).
                            OrderBy(a => a.Version).
                            OrderBy(a => a.ItemId).
                            OrderBy(a => a.AccountNumber).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }

            return result;
        }

        /// <summary>
        /// Parse one TransactionEvent object from one line of data
        /// </summary>
        /// <param name="rowdata"></param>
        /// <returns>TransactionEvent</returns>
        private static TransactionEvent FromRow(string rowdata)
        {
            TransactionEvent result = null;
            try
            {
                if (!string.IsNullOrEmpty(rowdata)) {
                    string[] nodes = rowdata.Split('|');//assum that the data is separated by single "|"
                    if (nodes.Length == COLUMN_COUNT) 
                    {
                        TransactionEvent transactionEvent = new TransactionEvent();
                        transactionEvent.AccountNumber = nodes[COLUMN_ACCOUNT];
                        transactionEvent.ItemId = nodes[COLUMN_ITEM_ID];

                        DirectionEnum direction;
                        if (Enum.TryParse(nodes[COLUMN_DIRECTION], true, out direction))
                        {
                            transactionEvent.Direction = direction;
                        }
                        OperationEnum operation;
                        if (Enum.TryParse(nodes[COLUMN_OPEATION], true, out operation))
                        {
                            transactionEvent.Operation = operation;
                        }
                        long longValue;
                        if (long.TryParse(nodes[COLUMN_QUANTITY], out longValue))
                        {
                            transactionEvent.Quantity = longValue;
                        }
                        if (long.TryParse(nodes[COLUMN_TRANSACTION_ID], out longValue))
                        {
                            transactionEvent.TransactionId = longValue;
                        }
                        if (long.TryParse(nodes[COLUMN_VERSION], out longValue))
                        {
                            transactionEvent.Version = longValue;
                        }

                        //only consider valid record only if all the data input is valid                    
                        if (transactionEvent.IsValid)
                        {
                            result = transactionEvent;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //log the issue here. in this assignment, no loging exception is implemented yet.
            }

            return result;
        }
    }
}
