﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXAModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CXADataBase
{
    public class FileDataHelper
    {
        public static bool SerializeObject(string filename, BaseSerialize objectToSerialize)
        {
            bool success = false;
            try
            {
                Stream stream = File.Open(filename, FileMode.Create);
                BinaryFormatter bFormatter = new BinaryFormatter();
                bFormatter.Serialize(stream, objectToSerialize);
                stream.Close();
                success = true;
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }
            return success;
        }

        public static BaseSerialize DeSerializeObject(string filename)
        {
            BaseSerialize objectToSerialize = null;
            try
            {
                Stream stream = File.Open(filename, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                objectToSerialize = (BaseSerialize)bFormatter.Deserialize(stream);
                stream.Close();
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }
            return objectToSerialize;

        }
    }
}
