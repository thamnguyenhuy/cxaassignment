﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CXAModel;
using System.IO;
using CXAUtil;
namespace CXADataBase
{
    public class FileDataBase
    {
        private static readonly FileDataBase m_Instance = null;

        private static List<Position> Positions = new List<Position>();
        static FileDataBase()
        {
            if (m_Instance == null)
            {
                m_Instance = new FileDataBase();
            }
        }

        public static FileDataBase Instance
        {
            get
            {
                return m_Instance;
            }
        }


        /// <summary>
        /// save the Position object to memory
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool SavePostion(Position input) {
            bool result = false;
            try
            {
                //search base on primary key:Account Number & Item Id
                Position existingPosition = Positions.Where(a => a.AccountNumber == input.AccountNumber && a.ItemId == input.ItemId).FirstOrDefault();
                if (existingPosition != null)
                {
                    //update the quatity & transaction id
                    existingPosition = input;
                }
                else 
                {
                    //insert new record to the Positions object
                    Positions.Add(input);
                }
                result = true;
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }

            return result;
        }

        /// <summary>
        /// get Position by account number and item Id
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public Position GetPostion(string accountNumber, string itemId)
        {
            Position result = null;
            try
            {
                //search base on primary key:Account Number & Item Id
                result = Positions.Where(a => a.AccountNumber.ToLower() == accountNumber.ToLower() && a.ItemId.ToLower() == itemId.ToLower()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }

            return result;
        }

        /// <summary>
        /// get list of Position from Memory
        /// </summary>
        /// <returns></returns>
        public List<Position> GetPostions()
        {
            return Positions;
        }

        /// <summary>
        /// update the list position.
        /// </summary>
        /// <param name="list"></param>
        public void UpdatePostions(List<Position> list)
        {
            Positions = list;
        }
    }
}
