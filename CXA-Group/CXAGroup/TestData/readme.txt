1) CXAGroup: the test client. 

2) CXABusiness: bussiness layer, responsible for the transaction event assignment & calculate position quantity. In addition, it is also responsible for the printing output to console

3) CXADataBase: responsible for storing Positions data to memory. In this assignment, I decided to use memory for storing db. However, we can use RDBMS for storing.

4) CXAModel: entity classes.

5) CXAParser: parser utility component responsibles for parsing the Transaction Event data from text file.

6) CXAUtil: another util component for general purpose, in this case, to read file content.
