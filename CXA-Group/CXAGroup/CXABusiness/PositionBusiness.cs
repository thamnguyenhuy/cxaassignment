﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXAModel;
using CXADataBase;

namespace CXABusiness
{
    public class PositionBusiness
    {
        /// <summary>
        /// To calculate the Postion object 's quantity
        /// </summary>
        /// <param name="list">List of Transaction Events</param>
        public static void CalculateQuantity(List<TransactionEvent> list)
        {
            try
            {
                if (list != null)
                {
                    //assign transaction event to position
                    foreach (TransactionEvent item in list)
                    {
                        AssignTransactionToPosition(item);
                    }

                    //get position from database (in this implementation, it is from memory)
                    List<Position> listPosition = FileDataBase.Instance.GetPostions();

                    //calculate the quanity base on its transaction event detail
                    listPosition = CalculateQuantity(listPosition);

                    //call db layer to store the data to memory
                    FileDataBase.Instance.UpdatePostions(listPosition);
                }
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller. have not implemented yet
            }
        }

        /// <summary>
        /// To assign transaction events to the Postion.
        /// </summary>
        /// <param name="transactionEvent"></param>
        private static void AssignTransactionToPosition(TransactionEvent transactionEvent)
        {
            try
            {
                if (transactionEvent != null)
                {
                    Position savedPosition = FileDataBase.Instance.GetPostion(transactionEvent.AccountNumber, transactionEvent.ItemId);

                    //there is one saved postion in db which has same account number & item id
                    if (savedPosition != null)
                    {
                        if (savedPosition.IsExistingTransactionEvent(transactionEvent))
                        {
                            //there is same transaction event ==> update
                            //update the transaction events
                            savedPosition.UpdateTransactionEvent(transactionEvent);
                        }
                        else
                        {
                            //there is NO transaction event yet ==> assign this to the Position
                            savedPosition.TransactionEvents.Add(transactionEvent); ////assign transaction event detail
                        }

                        //update to db
                        FileDataBase.Instance.SavePostion(savedPosition);
                    }
                    else
                    {
                        //there is NO saved postion in current db yet ==> insert new position record
                        Position position = new Position();
                        position.AccountNumber = transactionEvent.AccountNumber; //assign account number
                        position.ItemId = transactionEvent.ItemId;//assign item id
                        position.TransactionEvents.Add(transactionEvent); ////assign transaction event detail

                        //insert to db
                        FileDataBase.Instance.SavePostion(position);
                    }
                }
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }
        }

        /// <summary>
        /// Calculate the Position Quantity
        /// </summary>
        /// <param name="list"></param>
        /// <returns>List of Calculated Postions Quantity</returns>
        public static List<Position> CalculateQuantity(List<Position> list)
        {
            try
            {
                if (list != null)
                {
                    //assign transaction to position
                    foreach (Position item in list)
                    {
                        foreach (TransactionEvent transactionEvent in item.TransactionEvents)
                        {
                            //increase quantity if it is BUY, otherwise decrease.
                            item.Quantity += (transactionEvent.Direction == DirectionEnum.BUY ? transactionEvent.Quantity : -transactionEvent.Quantity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }

            return list;
        }

        /// <summary>
        /// Print result to the console output.
        /// </summary>
        public static void PrintResult()
        {
            try
            {
                Console.WriteLine("Account|Item|Quantity|Transactions");
                List<Position> Positions = FileDataBase.Instance.GetPostions();
                foreach (Position item in Positions)
                {
                    Console.WriteLine(item.ToString());
                }
            }
            catch (Exception ex)
            {
                //apply log4net to catch the exception here. or throw exception to caller
            }
        }
    }
}
