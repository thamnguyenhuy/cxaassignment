﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CXAUtil
{
    public class FileHelper
    {
        public static string ReadFile(string fileName)
        {
            string content = "";
            try
            {
                StreamReader oReader = File.OpenText(fileName);
                content = oReader.ReadToEnd();
                oReader.Close();
            }
            catch (Exception ex)
            {
                //log the issue here. in this assignment, no loging exception is implemented yet.
            }

            return content;
        }
    }
}
