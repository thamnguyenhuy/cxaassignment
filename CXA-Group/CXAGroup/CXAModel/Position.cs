﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXAModel
{
    public class Position
    {
        public string AccountNumber { get; set; }
        public string ItemId { get; set; }
        public long Quantity { get; set; }
        public List<TransactionEvent> TransactionEvents { get; set; }

        public Position()
        {
            TransactionEvents = new List<TransactionEvent>();
        }
        /// <summary>
        /// To override the base ToString method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string transid = "";
            foreach (TransactionEvent item in TransactionEvents)
            {
                transid += string.Format("{0},", item.TransactionId);
            }

            if (transid.Length >0){
                transid = transid.Substring(0, transid.Length - 1);
            }
            return string.Format("{0}|{1}|{2}|{3}", AccountNumber, ItemId, Quantity, transid);
        }

        /// <summary>
        /// To check if there is existing Transaction Event with same Transaction Id
        /// </summary>
        /// <param name="transactionEvent"></param>
        /// <returns></returns>
        public bool IsExistingTransactionEvent(TransactionEvent transactionEvent) 
        {
            bool result = false;
            if (transactionEvent != null && TransactionEvents != null)
            {
                foreach (TransactionEvent item in TransactionEvents)
                {
                    if (item.TransactionId == transactionEvent.TransactionId)
                    {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// To update the Transaction Event information
        /// </summary>
        /// <param name="transactionEvent"></param>
        /// <returns></returns>
        public bool UpdateTransactionEvent(TransactionEvent transactionEvent)
        {
            bool result = false;
            if (transactionEvent != null && TransactionEvents != null)
            {
                foreach (TransactionEvent item in TransactionEvents)
                {
                    //only update if same transaction id & the current verion is not the latest version
                    //it will solve the un-sequenced of the transtraction event rows in the input datafile.
                    if (item.TransactionId == transactionEvent.TransactionId && item.ItemId == transactionEvent.ItemId 
                        && item.Version < transactionEvent.Version)
                    {
                        item.Quantity = transactionEvent.Quantity;
                        item.Version = transactionEvent.Version;
                        item.Operation = transactionEvent.Operation;
                        item.Direction = transactionEvent.Direction;
                        break;
                    }
                }
            }
            return result;
        }
    }

    [Serializable()]
    public class PositionSerialize : BaseSerialize
    {
        public List<Position> Positions
        {
            get;
            set;
        }
    }
}
