﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXAModel
{
    public enum OperationEnum
    {
        NONE,
        NEW,
        AMEND,
        CANCEL
    }
    public enum DirectionEnum
    {
        NONE,
        BUY,
        SELL
    }

    public class TransactionEvent 
    {
        public long TransactionId { get; set; }
        public long Version { get; set; }
        public string ItemId { get; set; }
        public long Quantity { get; set; }
        public DirectionEnum Direction { get; set; }
        public string AccountNumber { get; set; }
        public OperationEnum Operation { get; set; }

        public TransactionEvent()
        {
            TransactionId = long.MinValue;
            Version = long.MinValue;
            Quantity = long.MinValue;
            AccountNumber = "";
            ItemId = "";
        }
        public bool IsValid
        {
            //asumming that only all the fields have data, then consider the item is valid, 
            //otherwise, the parser will ignore this row data
            get
            {
                return (TransactionId > long.MinValue && Version > long.MinValue
                    && !string.IsNullOrEmpty(ItemId) && Quantity > long.MinValue
                    && !string.IsNullOrEmpty(AccountNumber)
                    && Direction != DirectionEnum.NONE && Operation != OperationEnum.NONE);
            }
        }
    }
}
