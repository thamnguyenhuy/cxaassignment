﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXAModel;
using CXABusiness;
using System.Windows.Forms;
using CXAParser;
namespace CXAGroup
{
    class Program
    {
         [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Please select your transaction data file.");

            OpenFileDialog fileDialog = new OpenFileDialog();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                TransactionEventParserResult result = TransactionEventParser.FromFile(fileDialog.FileName);
                if (result.ErrorRows == null || result.ErrorRows.Count == 0)
                {
                    //all rows data are valid --> then process, otherwize, print errors out and exit.
                    PositionBusiness.CalculateQuantity(result.TransactionEvents);

                    //print the output
                    PositionBusiness.PrintResult();
                }
                else 
                { 
                    //inform client that there are some invalid row data
                    Console.WriteLine("There are errors in the transaction file as below: ");
                    foreach (string err in result.ErrorRows)
                    {
                        Console.WriteLine(err);
                    }
                }
             
                Console.Read();
            }
        }
    }
}
